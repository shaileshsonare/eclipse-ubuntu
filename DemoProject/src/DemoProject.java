
class Hi implements Runnable {

	public void run() {
		// TODO Auto-generated method stub
		for(int i = 0; i < 10; i++){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Hi");
		}
	}
}

class Hello extends Thread {

	public void run() {
		// TODO Auto-generated method stub
		for(int i = 0; i < 10; i++){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Hello");
		}
	}
}

class World extends Thread {

	public void run() {
		// TODO Auto-generated method stub
		for(int i = 0; i < 10; i++){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("World");
		}
	}
}

public class DemoProject {

	public static void main(String[] args) {
		Thread t1 = new Thread(new Hi());
		Hello t2 = new Hello();
		World t3 = new World();
		
		t1.start();
		t2.start();
		t3.start();
		
	}

}
