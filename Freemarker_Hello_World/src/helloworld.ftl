FreeMarker Template example: ${message}

<#macro greet>
  <font size="+2">Hello Joe!</font>
</#macro>  

<#macro greet1 person>
  <font size="+2">Hello ${person}!</font>
</#macro>

=======================
===  County List   ====
=======================
<#list countries as country>
	${country_index + 1}. ${country}
</#list>

Fruit List
<#list fruits as fruit>
	${fruit_index + 1} ${fruit}
</#list>

<@greet/>

<@greet1 person="Shailesh"/>